﻿using ChinskiListonosz.Core;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;


namespace ChinskiListonosz
{
    class Converter
    {
        private Boolean flag = false;
        private int counter = 0;
        private NumberFormatInfo numberFormat;

        public Converter()
        {
            numberFormat = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NegativeSign = "-"
        };
        }

        public string[] InitialPoint(string entry)
        {
            string[] point = new string[3];
            entry = entry.Remove(0, 2);
            string[] dummy = entry.Split(new char[] { ' ' });
            for (int i = 0; i < 3; i++)
            {
                point[i] = dummy[i];
            }
            return point;
        }

        //public void WriteText(List<KeyValuePair<Vector<float>, Vector<float>>> LinesList, string filePath)
        //{
        //    using (System.IO.StreamWriter file =
        //    new System.IO.StreamWriter(filePath))
        //    {
        //        var V = Vector<float>.Build;
        //        var FirstPoint = V.Random(3);
        //        var SecondPoint = V.Random(3);
        //        file.WriteLine("Number of Total Section Lines : " + LinesList.Count);
        //        file.WriteLine("=== Start of Section Line ===");
              
        //        for (int i = 0; i < LinesList.Count; i++)
        //        {
        //            FirstPoint = LinesList[i].Key;
        //            SecondPoint = LinesList[i].Value;
        //            file.WriteLine(FirstPoint.ToDouble()[0].ToString() + "\t" + (FirstPoint[1].ToString()) + "\t" + (FirstPoint[2].ToString()) + "\t" + SecondPoint[0].ToString() + "\t" + (SecondPoint[1].ToString()) + "\t" + (SecondPoint[2].ToString()));
        //        }
        //        file.WriteLine("=== End of Section Line ===");
        //    }
        //}

        public void WriteText(List<Edge> Edges, string filePath)
        {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(filePath))
            {
                string[] FirstPointCooridnates;
                string[] SecondPointCooridnates;
                file.WriteLine("Number of Total Section Lines : " + Edges.Count);
                file.WriteLine("=== Start of Section Line ===");

                for (int i = 0; i < Edges.Count; i++)
                {
                    FirstPointCooridnates = Edges[i].U.Split(new char[] { '|' });
                    SecondPointCooridnates = Edges[i].V.Split(new char[] { '|' });
                    file.WriteLine(FirstPointCooridnates[0] + "\t" + FirstPointCooridnates[1] + "\t" + FirstPointCooridnates[2] + "\t" + SecondPointCooridnates[0] + "\t" + SecondPointCooridnates[1] + "\t" + SecondPointCooridnates[2]);
                }
                file.WriteLine("=== End of Section Line ===");
            }
        }

        //public List<KeyValuePair<Vector<float>, Vector<float>>> ReadCurves(string file)
        //{
        //    //var fmt = new NumberFormatInfo();
        //    //fmt.NegativeSign = "-";
        //    string[] lines = File.ReadAllLines(file);
        //    List<KeyValuePair<Vector<float>, Vector<float>>> InitPointAndDir = new List<KeyValuePair<Vector<float>, Vector<float>>>();
        //    List<KeyValuePair<Vector<float>, Vector<float>>> LinesList = new List<KeyValuePair<Vector<float>, Vector<float>>>();
        //    List<KeyValuePair<int, string[]>> CurveNoAndParams = new List<KeyValuePair<int, string[]>>();
        //    ObservableCollection<string> Curves = new ObservableCollection<string>();
        //    foreach (string line in lines)
        //    {
        //        if (line.StartsWith("Curves"))
        //        {
        //            flag = true;
        //        }
        //        if (flag && !line.StartsWith("Polyg"))

        //        {
        //            Curves.Add(line);
        //        }
        //        else
        //        {
        //            flag = false;
        //        }

        //    }
        //    Curves.RemoveAt(0);
        //    var V = Vector<float>.Build;
        //    for (int i=0;i<Curves.Count;i++)
        //    {
        //        var point = V.Random(3);
        //        var dir = V.Random(3);
        //        var FirstPoint = V.Random(3);
        //        var SecondPoint = V.Random(3);
        //        string[] dummy = Curves[i].Split(new char[] { ' ' });
        //        for (int j = 0; j < 3; j++)
        //        {
        //            point[j] = float.Parse(dummy[j + 1], numberFormat);
        //            dir[j] = float.Parse(dummy[j + 4], numberFormat);
        //        }
        //        CurveNoAndParams = ReadCurveNoAndParams(file);
        //        string[] CurveParam = CurveNoAndParams[i].Value;
        //        InitPointAndDir.Add(new KeyValuePair<Vector<float>, Vector<float>>(point, dir));
        //        FirstPoint = InitPointAndDir[i].Key.Add((InitPointAndDir[i].Value.Multiply(float.Parse(CurveParam[0], numberFormat))));
        //        SecondPoint = InitPointAndDir[i].Key.Add((InitPointAndDir[i].Value.Multiply(float.Parse(CurveParam[1], numberFormat))));
        //        LinesList.Add(new KeyValuePair<Vector<float>, Vector<float>>(FirstPoint.PointwiseRound(), SecondPoint.PointwiseRound()));
        //    }
        //    return LinesList;
        //}

        public Graph ReadCurves(string file)
        {
            //var fmt = new NumberFormatInfo();
            //fmt.NegativeSign = "-";
            string[] lines = File.ReadAllLines(file);
            List<KeyValuePair<Vector<float>, Vector<float>>> InitPointAndDir = new List<KeyValuePair<Vector<float>, Vector<float>>>();
            List<KeyValuePair<Vector<float>, Vector<float>>> LinesList = new List<KeyValuePair<Vector<float>, Vector<float>>>();
            List<KeyValuePair<int, string[]>> CurveNoAndParams = new List<KeyValuePair<int, string[]>>();
            ObservableCollection<string> Curves = new ObservableCollection<string>();
            foreach (string line in lines)
            {
                if (line.StartsWith("Curves"))
                {
                    flag = true;
                }
                if (flag && !line.StartsWith("Polyg"))

                {
                    Curves.Add(line);
                }
                else
                {
                    flag = false;
                }

            }
            Curves.RemoveAt(0);
            var V = Vector<float>.Build;
            for (int i = 0; i < Curves.Count; i++)
            {
                var point = V.Random(3);
                var dir = V.Random(3);
                var FirstPoint = V.Random(3);
                var SecondPoint = V.Random(3);
                string[] dummy = Curves[i].Split(new char[] { ' ' });
                for (int j = 0; j < 3; j++)
                {
                    point[j] = float.Parse(dummy[j + 1], numberFormat);
                    dir[j] = float.Parse(dummy[j + 4], numberFormat);
                }
                CurveNoAndParams = ReadCurveNoAndParams(file);
                string[] CurveParam = CurveNoAndParams[i].Value;
                InitPointAndDir.Add(new KeyValuePair<Vector<float>, Vector<float>>(point, dir));
                FirstPoint = InitPointAndDir[i].Key.Add((InitPointAndDir[i].Value.Multiply(float.Parse(CurveParam[0], numberFormat))));
                SecondPoint = InitPointAndDir[i].Key.Add((InitPointAndDir[i].Value.Multiply(float.Parse(CurveParam[1], numberFormat))));
                LinesList.Add(new KeyValuePair<Vector<float>, Vector<float>>(FirstPoint.PointwiseRound(), SecondPoint.PointwiseRound()));
            }
            return GraphHelper.GraphRead(LinesList);
        }

        public Graph readFromText(string file)
        {
            try
            {
                List<KeyValuePair<Vector<float>, Vector<float>>> LinesList = new List<KeyValuePair<Vector<float>, Vector<float>>>();
            
            string[] lines = File.ReadAllLines(file);
  
            for (int i = 0; i < lines.Length - 3; i++)
            {
                var V = Vector<float>.Build;
                var FirstPoint = V.Random(3);
                var SecondPoint = V.Random(3);
                string lineChars = lines[i + 2].Replace(" ",""); 
                for (int k = 0; k < 6; k++)
                {
                    if (k < 3)
                    {
                        FirstPoint[k] = float.Parse(lineChars.Split('\t')[k].Replace("\t",""), numberFormat);
                    }
                    else
                    {
                        SecondPoint[k - 3] = float.Parse(lineChars.Split('\t')[k], numberFormat);
                    }
                }
                LinesList.Add(new KeyValuePair<Vector<float>, Vector<float>>(FirstPoint, SecondPoint));
            }
                return GraphHelper.GraphRead(LinesList);
            }
            catch (FormatException e)
            {
                Console.WriteLine("Please check the .txt file used, specifically line number where the drawing stopped");
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public List<KeyValuePair<int, string[]>> ReadCurveNoAndParams(string file)
        {
            string[] lines = File.ReadAllLines(file);
            List<KeyValuePair<int, string[]>> CurveNoAndLastPoint = new List<KeyValuePair<int, string[]>>();
            foreach (string line in lines)
            {
                if (line.StartsWith("Ed"))
                {
                    counter += 1;
                }
                else
                {
                    if (counter > 0)
                    {
                        switch (counter)
                        {
                            case 1:
                                counter += 1;
                                break;
                            case 2:
                                string[] values = line.Split(new char[] { ' ' });
                                int CurveNo = int.Parse(values[2]);
                                string[] CurveParamaMinAndMax = new string[2];
                                CurveParamaMinAndMax[0] = values[4];
                                CurveParamaMinAndMax[1] = values[5];
                                CurveNoAndLastPoint.Add(new KeyValuePair<int, string[]>(CurveNo, CurveParamaMinAndMax));
                                counter = 0;
                                break;
                        }
                    }
                    else
                    {

                    }
                }

            }

            return CurveNoAndLastPoint;
        }
    }
}
