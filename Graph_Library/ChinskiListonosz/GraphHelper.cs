﻿using ChinskiListonosz.Core;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace ChinskiListonosz
{

    class GraphHelper
    {
        private NumberFormatInfo numberFormat;
        private static Dictionary<int, Edge> edges;
        private Dictionary<int,string> vertices;

        public GraphHelper()
        {
            numberFormat = new NumberFormatInfo
            {
                NumberDecimalSeparator = ",",
                NegativeSign = "-"
            };
        }

        public static Graph GraphRead(List<KeyValuePair<Vector<float>, Vector<float>>> obj)
        {

            edges = new Dictionary<int,Edge>();
            for (int i = 0; i < obj.Count; i++)
            {
                var edge = new Edge(obj[i].Key[0].ToString()+"|" + obj[i].Key[1].ToString()+"|" + obj[i].Key[2].ToString() + "|", obj[i].Value[0].ToString() + "|" + obj[i].Value[1].ToString() + "|" + obj[i].Value[2].ToString() + "|", MathNet.Numerics.Distance.Euclidean(obj[i].Key,obj[i].Value),false);
                var tup = new Tuple<int, Edge>(i,edge);
                edges.Add(tup.Item1,tup.Item2);              
            }
            Graph graph = new Graph(edges.Values);
            return graph;
        }
}
}
