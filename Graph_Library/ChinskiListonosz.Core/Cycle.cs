﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinskiListonosz.Core
{
    public class Cycle
    {
        public Path cycle { get; set;}
        public double weight { get; set; }

        public Cycle(Path p)
        {
            cycle = p;
          CalculateWeight();
        }

        

        public void CalculateWeight()
        {
            foreach (Edge e in cycle.Edges)
            {
                weight += e.W;
            }
            weight = weight / cycle.Edges.Count;
        }
    }
}
