﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinskiListonosz.Core
{
    
        public class EdgeComparer : IEqualityComparer<Edge>
        {
            public bool Equals(Edge x, Edge y)
            {
               
                // Return true if the fields match:
                return (x.W == y.W
                        && ((x.U == y.U && x.V == y.V) || (x.U == y.V && x.V == y.U)));
            }

            public int GetHashCode(Edge y)
            {
                int hash = 13;
                hash = hash * 7 + y.U.GetHashCode();
                hash = y.V.GetHashCode();
                hash = hash * 7 + y.W.GetHashCode();
                return hash;
            }
        }
}
