﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ChinskiListonosz.Core.GraphBase;

namespace ChinskiListonosz.Core
{
    public interface IGraph
    {

       
         ///<summary>
         ///Returns a list of edges from the graph.
         ///</summary>
        List<Edge> Edges { get; }
        /// <summary>
        /// Returns an unordered list of vertices from the graph.
        /// </summary>
        List<string> Vertices { get; }
        /// <summary>
        /// Checks if the graph is connected.
        /// </summary>
        /// <returns></returns>
        bool IsConnected { get; }
        /// <summary>
        /// Creates a list of degrees of vertices.
        /// </summary>
        /// <returns>List of pairs (vertex number, vertex degree).</returns>
        List<Tuple<string, int>> Degrees();
        /// <summary>
        /// Returns number of vertices in graph.
        /// </summary>
        int NumberOfVertices { get; }
        /// <summary>
        /// Returns number of edges in graph.
        /// </summary>
        int NumberOfEdges{ get; }
        /// <summary>
        /// Calculates shortest paths between all vertices in graph.
        /// </summary>
        /// <returns>List of Paths connecting different vertices.</returns>
        List<Path> Distances();
        /// <summary>
        /// Creates a subgraph with all existing edges between given vertices.
        /// </summary>
        /// <param name="vertices">List of vertices to be included in subgraph.</param>
        /// <returns>A graph with all given vertices and all edges from original graph that connect vertices from given set.</returns>
        IGraph Subgraph(List<string> vertices);

        void Union(Subset[] subsets, string x, string y);
        string Find(Subset[] subsets, string i);
        void AddVertice(string v);
        void RemoveVertice(string v);
        void AddEdge(Edge e);
        void AddEdge(int position, Edge e);
        void RemoveEdge(Edge e);
    }
}