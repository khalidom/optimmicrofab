﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinskiListonosz.Core.Algorithms
{
    public static partial class GraphAlgorithms
    {
        static List<Cycle> Cycles; 
        public static Path EulerCycle(this IGraph graph, string start)
        {
            if (graph.Degrees().Any(tp => tp.Item2.IsOdd()))
            {
                throw new ArgumentException("Graph is not an Euler Graph");
            }
            var V = Vector<float>.Build;
            var FirstPoint = V.Random(3);
            var StartPoint = V.Random(3);
            var FirstPoint2 = V.Random(3);
            string[] StartPointCooridnates = start.Split(new char[] { '|' });
            for (int i = 0; i < graph.Edges.Count; i++)
            {

                string[] FirstPointCooridnates = graph.Edges[i].U.Split(new char[] { '|' });
                string[] FirstPointCooridnates2 = graph.Edges[i].V.Split(new char[] { '|' });

                for (int k = 0; k < 3; k++)
                {
                    FirstPoint[k] = float.Parse(FirstPointCooridnates[k]);
                    StartPoint[k] = float.Parse(StartPointCooridnates[k]);
                    FirstPoint2[k] = float.Parse(FirstPointCooridnates2[k]);
                }

                //facons differentes de ponderer les segments meme si cela n'a pas bcp d'effet
                //graph.Edges[i].W = MathNet.Numerics.Distance.Euclidean(FirstPoint, StartPoint) +
                //    MathNet.Numerics.Distance.Euclidean(FirstPoint2, StartPoint);
                //graph.Edges[i].W += FirstPoint2[2] - StartPoint[2];
                //graph.Edges[i].W += FirstPoint[2] - StartPoint[2];
                //graph.Edges[i].W = graph.Edges[i].W + Math.Abs(FirstPoint2[2] - StartPoint[2]) +
                //    +Math.Abs(FirstPoint[2] - StartPoint[2]);
                graph.Edges[i].W = graph.Edges[i].W - (FirstPoint2[2] - FirstPoint[2]);
            }

            List<Edge> remainingEdges = graph.Edges.OrderBy(p=>p.W).ToList();

            Path result = FindCycle(remainingEdges, start);
            Debug.WriteLine(result.ToString());
            while (remainingEdges.Count > 0)
            {              
                foreach (Edge edge in remainingEdges)
                {
                    var temp = result.VisitedVertices.Find(i => edge.IsIncident(i));
                    //var temp = result.VisitedVertices.Where(i => edge.IsIncident(i)).OrderBy(k => double.Parse(k.Split(new char[] { '|' })[2])).ToList().FirstOrDefault();
                    if (temp != null)
                    {
                        start = temp.ToString();
                        break;
                    }

                }
                result.InsertAtSuitable(FindCycle(remainingEdges, start));
              
              
            }
          

            return result;
        }

        private static Path FindCycle(List<Edge> edges, string start)
        {
            double weight = 0;
            Path result = new Path(start);
            Edge nextEdge;
            double minValue;
            string nextV = start;
           while (nextV != start || result.Length == 0)
            {
               minValue = edges.Where(e => e.IsIncident(nextV)).ToList().Select(j=>j).Min(x => x.W);
               nextEdge = edges.Where(e => e.IsIncident(nextV)).Where(r=>r.W==minValue).ToList().OrderBy(f => Guid.NewGuid()).Take(1).First();         
               result.AddToEnd(nextEdge);
                nextV = nextEdge.OtherEndTo(nextV);
                edges.Remove(nextEdge);
            }
            //foreach (Edge u in result.Edges)
            //{
            //    weight += u.W;
            //}
           // Debug.WriteLine("circuit!!!!!");
            //Cycles.Add(new Cycle(result));
            return result;
        }
    }
}
