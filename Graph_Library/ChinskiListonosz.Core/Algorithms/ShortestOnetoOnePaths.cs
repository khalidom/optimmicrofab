﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChinskiListonosz.Core.Algorithms
{
    public static partial class GraphAlgorithms
    {
        private static List<string> OddV;
        private static List<string> visitedVertices;
        private static List<Edge> result;

        public static Graph ShortestOnetoOnePaths(List<string> Odds)
        {
            result = new List<Edge>();
            OddV = Odds;
            visitedVertices = new List<string>(OddV.Count);
            foreach (string vertex in OddV.Where(p => !visitedVertices.Contains(p)))
                {              
                    string EndVertex =  ComputeMinimumDistance(vertex);
                if (vertex != EndVertex)
                {
                    BuildEdge(vertex, EndVertex);
                    visitedVertices.Add(vertex);
                    visitedVertices.Add(EndVertex);
                }
            };
            return new Graph(new List<Edge>(result));
        }

        private static void BuildEdge(string vertex, string endVertex)
        {
            string[] FirstPointCooridnates = vertex.Split(new char[] { '|' });
            string[] SecondPointCooridnates = endVertex.Split(new char[] { '|' });
            var V = Vector<float>.Build;
            var FirstPoint = V.Random(FirstPointCooridnates.Length-1);
            var SecondPoint = V.Random(FirstPointCooridnates.Length-1);
           
            for (int k = 0; k < FirstPointCooridnates.Length-1; k++)
            {
                FirstPoint[k] = float.Parse(FirstPointCooridnates[k]);
                SecondPoint[k] = float.Parse(SecondPointCooridnates[k]);
            }
            var edge = new Edge(vertex, endVertex, MathNet.Numerics.Distance.Euclidean(FirstPoint, SecondPoint),true);
            result.Add(edge);
        }

        private static string ComputeMinimumDistance(string vertex)
        {
            Dictionary<string,double> dists = new Dictionary<string,double>();
            foreach (string PossiblEndVertex in OddV.Where(p => !visitedVertices.Contains(p)&& p!=vertex))
            {
                string[] FirstPointCooridnates = vertex.Split(new char[] { '|' });
                string[] SecondPointCooridnates = PossiblEndVertex.Split(new char[] { '|' });
                var V = Vector<float>.Build;
                var FirstPoint = V.Random(FirstPointCooridnates.Length-1);
                var SecondPoint = V.Random(FirstPointCooridnates.Length-1);
               
                for (int k = 0; k < FirstPointCooridnates.Length-1; k++)
                {
                    FirstPoint[k] = float.Parse(FirstPointCooridnates[k]);
                    SecondPoint[k] = float.Parse(SecondPointCooridnates[k]);
                }
                //We compute distances between odd-connections segments and we penalize connecting from a level to a higher 
                //level in Z direction. We reward going doing on the same axis
                dists.Add(PossiblEndVertex,MathNet.Numerics.Distance.Euclidean(FirstPoint, SecondPoint)+SecondPoint[2]- FirstPoint[2]);
            }
            return dists.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
        }
    }
}




