﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ChinskiListonosz.Core.GraphBase;
using static ChinskiListonosz.Core.IGraph;

namespace ChinskiListonosz.Core.Algorithms
{
    public static partial class GraphAlgorithms
    {

        //public struct Subset
        //{
        //    public string Parent;
        //    public int Rank;
        //}

        public static Graph Kruskal(this IGraph graph, IEnumerable<Edge> AllEdges)
        {

            var kruskal = new List<Edge>();
            var forest = new List<List<string>>();
            int uIndex = 0;
            int vIndex = 0;

            foreach (var edge in graph.Edges.OrderBy(e => e.W).ThenBy(f=>Guid.NewGuid()))
            {
                var indexes = FindSubTreesIndexes(forest, edge);
                uIndex = indexes.Key;
                vIndex = indexes.Value;
                if (uIndex == -1 && vIndex == -1)
                {
                    forest.Add(new List<string> { edge.U, edge.V });
                    kruskal.Add(edge);
                }
                else if (uIndex == -1)
                {
                    forest[vIndex].Add(edge.U);
                    kruskal.Add(edge);
                }
                else if (vIndex == -1)
                {
                    forest[uIndex].Add(edge.V);
                    kruskal.Add(edge);
                }
                else if (uIndex != vIndex)
                {
                    forest[uIndex].AddRange(forest[vIndex]);
                    forest.RemoveAt(vIndex);
                    kruskal.Add(edge);
                }
            }
            
            var evenV = new Graph(new HashSet<Edge>(kruskal)).Degrees()
                       .Where(vDeg => vDeg.Item2.IsEven())
                       .Select(tup => tup.Item1)
                       .ToList();

            for (int i = 0; i < evenV.Count(); i += 2)
            {
               try
                {
                     var edgeExist= kruskal.Single(path => path.U == evenV[i] && path.V== evenV[i + 1]);
                }
                catch (System.InvalidOperationException e)
                {
                    var V = Vector<float>.Build;
                    var FirstPoint = V.Random(3);
                    var SecondPoint = V.Random(3);
                    string[] FirstPointCooridnates = evenV[i].Split(new char[] { '|' });
                    string[] SecondPointCooridnates = evenV[i+1].Split(new char[] { '|' });
                    for (int k = 0; k < 3; k++)
                    {                       
                            FirstPoint[k] = float.Parse(FirstPointCooridnates[k]);
                            SecondPoint[k] = float.Parse(SecondPointCooridnates[k]);                        
                    }
                    var edge = new Edge(evenV[i], evenV[i + 1], MathNet.Numerics.Distance.Euclidean(FirstPoint, SecondPoint),true);                   
                    kruskal.Add(edge);
                }
            
            }
                return new Graph(new HashSet<Edge>(kruskal));
        }
    
        private static KeyValuePair<int, int> FindSubTreesIndexes(List<List<string>> forest, Edge edge)
        {
            int uIndex = -1;
            int vIndex = -1;
            foreach (List<string> subTreeVertices in forest)
            {
                if (subTreeVertices.Contains(edge.U))
                    uIndex = forest.IndexOf(subTreeVertices);

                if (subTreeVertices.Contains(edge.V))
                    vIndex = forest.IndexOf(subTreeVertices);
            }
            return new KeyValuePair<int, int>(uIndex, vIndex);
        }
    }
}




