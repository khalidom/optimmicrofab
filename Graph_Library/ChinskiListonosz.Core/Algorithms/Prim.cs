﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ChinskiListonosz.Core.GraphBase;
using static ChinskiListonosz.Core.IGraph;

namespace ChinskiListonosz.Core.Algorithms
{
    public static partial class GraphAlgorithms
    {

      
            private static Edge[] _edgeTo; //Keep track of the edges in our minimum spanning tree      
            private static double[] _distTo; //Keep track of the weights to each edge in our minimum spanning tree   
            private static Boolean[] _marked; //Keep track of which vertex we've looked
            //private static IndexMinPriorityQueue<Double> _pq;  //the [vertex number]|[weight] key value pairs in our minimum spanning tree

            public Graph PrimMST(Graph G)
            {
                //initialize the various arrays and the minimum priority queue
                _edgeTo = new Edge[G.V()];
                _distTo = new double[G.V()];
                _marked = new Boolean[G.V()];
                //_pq = new IndexMinPriorityQueue<Double>(G.V());
                //for each edge in the minimum spanning tree set the weight equal to infinity
                for (int v = 0; v < G.Edges.NumberOfEdges; v++) _distTo[v] = Double.PositiveInfinity;

                for (int v = 0; v < G.NumberOfEdges; v++)
                    if (!_marked[v]) Prim(G, v);
            }

            private void Prim(Graph G, int s)
            {
                //set the weight to source vertex s as 0
                _distTo[s] = 0.0;
                //insert the vertex into the priority queue as (vertex number, weight)
                _pq.Insert(s, _distTo[s]);
                while (!_pq.IsEmpty())
                {
                    //remove a vertex from the top of the queue
                    int v = _pq.DeleteMin();
                    Scan(G, v);
                }
            }

            /*
            * This method takes a vertex v, finds all the vertices connected to v and 
            * compares their weights to the weights we've already found and determines if any of the weights
            * are less than what we already have
            * */
            private void Scan(Graph G, int v)
            {
                //mark the vertex v, we only want one instance of each vertex in the mst
                _marked[v] = true;
                /*
                * for each edge connected to v
                *      -get the target vertex (w)
                *      -compare the weight from s to w,
                *      -if the weight from s to w is less than the weight from any other vertex to w
                *       that we've already encountered 
                *       -replace 
                *          _distTo[w] with the new value 
                *          _edgeTo[w] with the new edge
                *          _pq[w] with the new distance or insert it into _distTo[] if a value
                *          for_distTo[w] doesn't already exist
                * */
                foreach (Edge e in G.Adj(v))
                {
                    int w = e.Target(v);
                    if (_marked[w]) continue;
                    if (e.Weight() < _distTo[w])
                    {
                        _distTo[w] = e.Weight();
                        _edgeTo[w] = e;
                        if (_pq.Contains(w)) _pq.ChangeKey(w, _distTo[w]);
                        else _pq.Insert(w, _distTo[w]);
                    }
                }
            }

            //Return all the edges in the MST
            public IEnumerable<Edge> Edges()
            {
                Queue<Edge> mst = new Queue<Edge>();
                for (int v = 0; v < _edgeTo.Length; v++)
                {
                    Edge e = _edgeTo[v];
                    if (e != null)
                    {
                        mst.Enqueue(e);
                    }
                }
                return mst;
            }

            /*
            * Return the total weight of the of the minimum spanning tree.  The weight
            * should be no larger than the weight of any other spanning tree.
            * */
            public double Weight()
            {
                double weight = 0.0;
                foreach (Edge e in Edges())
                    weight += e.Weight();
                return weight;
            }


        
}






