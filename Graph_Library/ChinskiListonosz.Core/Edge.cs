﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinskiListonosz.Core
{
    public class Edge
    {

        public string U { get; set; }
        public string V { get; set; }
        public double W { get; set; }
        //white edges are the edges that won't be actually printed out by the microfabrication machine.
        public bool WhiteEdge { get; set; }

        public Edge(string u, string v, double w, bool whiteEdge)
        {
            U = u;
            V = v;
            W = w;
            WhiteEdge = whiteEdge;

        }

        public Edge(Edge e) : this(e.U, e.V, e.W, e.WhiteEdge) { }
        public Edge Clone() { return new Edge(this); }

        public Edge Flip()
        {
            return new Edge(this.V, this.U, this.W, this.WhiteEdge);
        }

        public void Flip(bool self)
        {
            var u = this.U;
            this.U = this.V;
            this.V = u;
        }

        public bool IsIncident(string v)
        {
            return (U == v || V == v);
        }
        public bool IsIncident(Edge e)
        {
            return (e.U == U || e.U == V || e.V == U || e.V == V);
        }

        public string OtherEndTo(string u)
        {
            if (U == u)
                return V;
            return U;
        }

        public override string ToString()
        {
            return string.Format("{0} -> {1} ({2})", U, V, W);
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Edge return false.
            Edge otherEdge = obj as Edge;
            return this.Equals(otherEdge);
        }

        public bool Equals(Edge otherEdge)
        {
            // If parameter is null return false:
            if ((object)otherEdge == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (W == otherEdge.W 
                    && ((U == otherEdge.U && V == otherEdge.V) || (U == otherEdge.V && V == otherEdge.U))&& WhiteEdge==otherEdge.WhiteEdge);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            int hash = 13;
            hash = hash * 7 + U.GetHashCode();
            hash = hash * 7 + V.GetHashCode();
            hash = hash * 7 + W.GetHashCode();
            return hash;
        }
    }
}
