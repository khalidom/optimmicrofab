﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinskiListonosz.Core
{
    public class MultiGraph : GraphBase
    {
        protected List<Tuple<Edge, int>> edges = new List<Tuple<Edge, int>>();
        public override List<Edge> Edges
        {
            get
            {
               return edges.SelectMany(edgeN => Enumerable.Repeat(edgeN.Item1, edgeN.Item2)).ToList();
            }

        }
        
        public MultiGraph(IEnumerable<string> V, IEnumerable<Edge> E)
        {
            if (E.Any(e => !V.Contains(e.U) || !V.Contains(e.V)))
                throw new ArgumentException("Edges can only connect Vertices from V!");
            vertices = new HashSet<string>(V);
            foreach (Edge r in E)
            {
                edges.Add(new Tuple<Edge, int>(r,1));
            }
        }
        public MultiGraph(IEnumerable<Edge> E) : this( E.SelectMany(e => new string[] { e.U, e.V }), E) { }
        public MultiGraph(IGraph G) : this(G.Vertices, G.Edges) { }

        public override int NumberOfEdges { get { return edges.Sum(edgeN => edgeN.Item2); } }
        public override int NumberOfVertices { get { return vertices.Count(); } }


        public override IGraph Subgraph(List<string> vertices)
        {
            throw new NotImplementedException();
        }

        public override void RemoveVertice(string v)
        {
            vertices.Remove(v);
            foreach(var edgeN in edges.Where(e => e.Item1.IsIncident(v)))
            {
                edges.Remove(edgeN);
            }
        }

        public override void AddEdge(Edge e)
        {
            //if (edges.ContainsKey(e))
            //    edges[e]++;
            //else
            //    edges.Add(e, 1);
        }
        public override void AddEdge(int position,Edge e)
        {
            //if (edges.ContainsKey(e))
            //    edges[e]++;
            //else
            //    edges.Add(e, 1);
        }

        public override void RemoveEdge(Edge e)
        {
            //if (edges[e] == 1)
            //    edges.Remove(e);
            //else
            //    edges[e]--;
        }

        protected override int[] DegreesFromEdges(List<string> vertices)
        {
            var degrees = new int[NumberOfVertices];
            foreach (var edgeN in edges)
            {
                degrees[vertices.IndexOf(edgeN.Item1.U)] += edgeN.Item2;
                degrees[vertices.IndexOf(edgeN.Item1.V)] += edgeN.Item2;
            }

            return degrees;
        }
    }
}
