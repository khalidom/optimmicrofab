﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinskiListonosz.Core
{
    public abstract class GraphBase : IGraph
    {
        protected HashSet<string> vertices = new HashSet<string>();
        public List<string> Vertices { get { return vertices.Distinct().ToList(); } }
        public abstract List<Edge> Edges { get;}

        public struct Subset
        {
            public string Parent;
            public int Rank;
        }


        public bool IsConnected
        {
            get
            {
                var v = Vertices.First();
                var reachable = new List<string>();
                var newReachable = new List<string>() { v };
                while (newReachable.Count > 0)
                {
                    reachable = reachable.Union(newReachable).ToList();
                    newReachable = newReachable.SelectMany(u => ConnectedTo(u)).ToList();
                    newReachable = newReachable.Except(reachable).ToList();
                }
                if (Vertices.All(vertex => reachable.Contains(vertex)))
                    return true;
                return false;
            }
        }

        public string Find(Subset[] subsets, string i)
        {
            if (subsets[Vertices.IndexOf(i)].Parent != i)
                subsets[Vertices.IndexOf(i)].Parent = Find(subsets, subsets[Vertices.IndexOf(i)].Parent);

            return subsets[Vertices.IndexOf(i)].Parent;
        }

        public void Union(Subset[] subsets, string x, string y)
        {
            string xroot = Find(subsets, x);
            string yroot = Find(subsets, y);

            if (subsets[Vertices.IndexOf(xroot)].Rank < subsets[Vertices.IndexOf(yroot)].Rank)
                subsets[Vertices.IndexOf(xroot)].Parent = yroot.ToString();
            else if (subsets[Vertices.IndexOf(xroot)].Rank > subsets[Vertices.IndexOf(yroot)].Rank)
                subsets[Vertices.IndexOf(yroot)].Parent = xroot.ToString();
            else
            {
                subsets[Vertices.IndexOf(yroot)].Parent = xroot.ToString();
                ++subsets[Vertices.IndexOf(yroot)].Rank;
            }
        }

        private List<string> ConnectedTo(string u)
        {
            return Edges.Where(e => e.IsIncident(u)).Select(e => e.OtherEndTo(u)).Distinct().ToList();
        }

        public abstract int NumberOfEdges { get; }
        public abstract int NumberOfVertices { get; }
        
        public List<Tuple<string, int>> Degrees()
        {
            var result = new List<Tuple<string, int>>();
            var verts = Vertices;
            var degrees = DegreesFromEdges(verts);
            
            for (int i = 0; i < NumberOfVertices; i++)
            {
                result.Add(new Tuple<string, int>(verts[i], degrees[i]));
            }
            return result;
        }

        protected abstract int[] DegreesFromEdges(List<string> vertices);

        public List<Path> Distances()
        {
            List<Path> allDist;
            allDist = new List<Path>();
            foreach (var startVertex in Vertices)
            {
                allDist.AddRange(Dijkstra(startVertex));
            }
            return allDist.Distinct().ToList();
        }

        protected List<Path> Dijkstra(string startVertex)
        {
            var numberFormat = new NumberFormatInfo
            {
                NumberDecimalSeparator = ",",
                NegativeSign = "-"
            };

            int[] dist = new int[NumberOfVertices];
            string[] prev = new string[NumberOfVertices];
            var unvisited = this.Vertices;

            for (int i = 0; i < NumberOfVertices; i++)
            {
                dist[i] = int.MaxValue; ;
            }

            dist[Vertices.IndexOf(startVertex)] = 0;
            var v = startVertex;

            while (unvisited.Count > 0)
            {
                var edgeList = this.Edges
                    .Distinct()
                    .Where(e => e.IsIncident(v))
                    .Where(e => unvisited.Contains(e.OtherEndTo(v)))
                    .OrderBy(e => e.W)
                    .ToList();

                foreach (var e in edgeList)
                {
                    var u = e.OtherEndTo(v);
                    if (dist[Vertices.IndexOf(v)]+ (int) Math.Round(double.Parse(e.W.ToString(), numberFormat)) < dist[Vertices.IndexOf(u)])
                    {
                        dist[Vertices.IndexOf(u)] = ((int) Math.Round(double.Parse(e.W.ToString(), numberFormat))+ dist[Vertices.IndexOf(v)]);
                        prev[Vertices.IndexOf(u)] = v;
                    }
                }

                unvisited.Remove(v);
                v = unvisited.OrderBy(ver => dist[unvisited.IndexOf(ver)].ToString()).FirstOrDefault();
            }

            return RecreatePaths(startVertex, prev);
        }

        protected List<Path> RecreatePaths(string startVertex, string[] prev)
        {
            var paths = new List<Path>();
            foreach (var ver in this.Vertices)
            {
                if (ver != startVertex)
                {
                    Path p = new Path(ver);
                    var a = ver;
                    var b = prev[Vertices.IndexOf(ver)];

                    while (a != startVertex && b != null)
                    {
                        p.AddToStart(this.Edges.First(e => e.IsIncident(a) && e.IsIncident(b.ToString())));
                        a = b.ToString();
                        b = prev[Vertices.IndexOf(a)];
                    }
                    if (a == startVertex)
                        paths.Add(p);
                }
            }
            return paths;
        }

        public abstract IGraph Subgraph(List<string> vertices);

        public void AddVertice(string v)
        {
            vertices.Add(v);
        }
        public abstract void RemoveVertice(string v);
        public abstract void AddEdge(Edge e);
        public abstract void RemoveEdge(Edge e);
        public abstract void AddEdge(int position,Edge e);
    }
}
