The OptimMicroFab repository contains two projects both written in .NET/C#. The first one codes the user interface that is responsible for:
- Opening and saving 3D wireframe models with .txt and .breb.
- Visualizing and manipulating the 3D model.
- Adding or deleting segments directly.
- Performing the modified Chinese Posteman algorithm.
- Animating the trajectories in order to see how they executes.
In the Graph_Library folder we find the second project. As the name suggests, it codes the necessary functions that is consumed by the user interface to be able to perform operations related to segments and graph theory functions. This code is based on a previous Github repository that has been deleted since.

