﻿using MathNet.Numerics.LinearAlgebra;
using System.Collections.Generic;
using System;
using ChinskiListonosz.Core;

namespace WireframeBrebConverter
{
    public class SegmentsComparer : IEqualityComparer<KeyValuePair<Vector<float>, Vector<float>>>
    {
        public bool Equals(KeyValuePair<Vector<float>, Vector<float>> x, KeyValuePair<Vector<float>, Vector<float>> y)
        {
            return x.Key.Equals(y.Key) && x.Value.Equals(y.Value)
                    ||
                    x.Key.Equals(y.Value) && x.Value.Equals(y.Key);
        }

        public int GetHashCode(KeyValuePair<Vector<float>, Vector<float>> obj)
        {
            int hashKey = obj.Key.GetHashCode();
            int hashVal = obj.Value.GetHashCode();
            return hashKey ^ hashVal;
        }
    }
}
