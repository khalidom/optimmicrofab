﻿using ChinskiListonosz.Core;
using ChinskiListonosz.Core.Algorithms;
using HelixToolkit.Wpf;
using MathNet.Numerics.LinearAlgebra;
using Microsoft.Win32;
using SciColorMaps;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Media3D;

namespace WireframeBrebConverter
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        ///  PauseToke is a class used to halt the execution of the program, in this case it stops the animation
        public class PauseTokenSource
        {
            private volatile TaskCompletionSource<bool> m_paused;

            public bool IsPaused
            {
                get { return m_paused != null; }
                set
                {
                    if (value)
                    {
                        Interlocked.CompareExchange(
                            ref m_paused, new TaskCompletionSource<bool>(), null);
                    }
                    else
                    {
                        while (true)
                        {
                            var tcs = m_paused;
                            if (tcs == null) return;
                            if (Interlocked.CompareExchange(ref m_paused, null, tcs) == tcs)
                            {
                                tcs.SetResult(true);
                                break;
                            }
                        }
                    }
                }
            }

            internal Task WaitWhilePausedAsync()
            {
                var cur = m_paused;
                return cur != null ? cur.Task : s_completedTask;
            }

            internal static readonly Task s_completedTask = Task.FromResult(true);

            public PauseToken Token { get { return new PauseToken(this); } }
        }
        public struct PauseToken
        {
            private readonly PauseTokenSource m_source;
            internal PauseToken(PauseTokenSource source) { m_source = source; }

            public bool IsPaused { get { return m_source != null && m_source.IsPaused; } }

            public Task WaitWhilePausedAsync()
            {
                return IsPaused ?
                    m_source.WaitWhilePausedAsync() :
                    PauseTokenSource.s_completedTask;
            }
        }


        PauseTokenSource pts = new PauseTokenSource();
        static ManualResetEventSlim  mres = new ManualResetEventSlim(false);
        private int OptStartPointIndex;
        List<Edge> li;
        private Path answer;
        private int animationTimeDelay = 840;
        private bool OnAnimation = false;
        private NumberFormatInfo numberFormat;
        private string lineString = "______"; //this appears with the segment number in the list found on the fat left in the user interface, change the length by adding or subtracting any number of(_)
        private ObservableCollection<Color> _colors;
        private String filePath;
        private ColorMap cmap;
        public List<KeyValuePair<Vector<float>, Vector<float>>> LinesList;
        Graph MainG;

        private string _FirstSelectedPoint { get; set; }
        private string _SecondSelectedPoint { get; set; }

        public MainWindow()
        {
            
            InitializeComponent();
            LinesList = new List<KeyValuePair<Vector<float>, Vector<float>>>();
            _colors = new ObservableCollection<Color>();
            OptStartPointIndex = 1; // this is just an initialization to the starting point of optimization, could be changed by selecting the desired segment in the list shown on the UI

            var colorsMaps = new[]
           {
                new byte[] {0, 0, 255},
                new byte[] { 0, 255, 0},
                new byte[] {255, 0, 0},
                
                new byte[] {0, 0,0}
            };
            var positions = new[] { 0, 0.3f, 0.7f, 1 };

           cmap = ColorMap.CreateFromColors(colorsMaps, positions, colorCount: 256);
           // cmap = new ColorMap();
            LinesOrder.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
             numberFormat = new NumberFormatInfo
            {
                NumberDecimalSeparator = "."
            };
            this.DataContext = this;
        }

        void ItemContainerGenerator_StatusChanged(object sender, EventArgs e)
        {
            if (LinesOrder.ItemContainerGenerator.Status
                == System.Windows.Controls.Primitives.GeneratorStatus.ContainersGenerated)
            {
                LinesOrder.ItemContainerGenerator.StatusChanged
                    -= ItemContainerGenerator_StatusChanged;
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Input,
                    new Action(DelayedAction));
            }
        }

        //used to control some segments visual features during the animation for more clarity
        void DelayedAction()
        {
            for (int i = 0; i < colors.Count; i++)
            {
                ListBoxItem lbi = LinesOrder.ItemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
                if (lbi != null)
                {
                    lbi.Foreground = new SolidColorBrush(colors[i]);
                    lbi.FontWeight = FontWeights.Bold;
                    lbi.FontSize = 30;
                }
            }
        }

       

        public string FirstSelectedPoint
        {
            get
            {
                return _FirstSelectedPoint;
            }
            set
            {
                _FirstSelectedPoint = value;
                this.NotifyPropertyChanged("FirstSelectedPoint");
            }
        }

        public string SecondSelectedPoint
        {
            get
            {
                return _SecondSelectedPoint;
            }
            set
            {
                _SecondSelectedPoint = value;
                this.NotifyPropertyChanged("SecondSelectedPoint");
            }
        }

        public ObservableCollection<Color> colors
        {
            get
            {
                return _colors;
            }
            set
            {
                _colors = value;
                this.NotifyPropertyChanged("colors");
            }
        }

        //read files with .breb or .txt containing the list of lie segments in the model
        private void Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Breb files (*.brep;*.brp;*.txt)|*.brep;*.brp;*.txt";
            if (openFileDialog.ShowDialog() == true)
            {
                if (System.IO.Path.GetExtension(openFileDialog.FileName).Equals(".brep") || System.IO.Path.GetExtension(openFileDialog.FileName).Equals(".brp"))
                {
                    LinesOrder.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
                    filePath = openFileDialog.FileName;
                    Converter converter = new Converter();
                    MainG = converter.ReadCurves(filePath);
                    LinesList = (List<KeyValuePair<Vector<float>, Vector<float>>>)LinesList.Distinct(new SegmentsComparer()).ToList();
                    CreateScene(true, 0);                  
                }
                else if (System.IO.Path.GetExtension(openFileDialog.FileName).Equals(".txt"))
                {
                    LinesOrder.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
                    filePath = openFileDialog.FileName;
                    Converter converter = new Converter();
                    MainG = converter.readFromText(filePath);
                    LinesList = (List<KeyValuePair<Vector<float>, Vector<float>>>)LinesList.Distinct(new SegmentsComparer()).ToList();
                    CreateScene(true, 0);                   
                }                              
            }
            
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text file (*.txt)|*.txt";
            if (saveFileDialog.ShowDialog() == true)
            {
                SaveWireToText(saveFileDialog.FileName);
            }
        }

        private void SaveWireToText(string file)
        {
         Converter converter = new Converter();
         converter.WriteText(MainG.Edges.Where(e => e.WhiteEdge == false).ToList(), file);
        }

        private int Normalization(int value, int max )
        {
            return (int)Math.Floor((double)(255/max) * value); 
        }
       

        public void CreateScene(bool IsWithColor, int indx)
        {

            NumberFormatInfo numberFormat = new NumberFormatInfo
            {
                NumberDecimalSeparator = "."
            };
            Model.Children.Clear();
            LinesOrder.Items.Clear();
            if (IsWithColor)
            {
                colors.Clear();              
            }
            ArrowVisual3D line;
            TextVisual3D txt;
            var V = Vector<float>.Build;
            var FirstPoint = V.Random(3);
            var SecondPoint = V.Random(3);
           
                for (int i = 0; i < MainG.Edges.Count(); i++)
                {
                string[] FirstPointCooridnates = MainG.Edges[i].U.Split(new char[] { '|' });
                string[] SecondPointCooridnates = MainG.Edges[i].V.Split(new char[] { '|' });
                for (int k = 0; k < 3; k++)
                {
                    FirstPoint[k] = float.Parse(FirstPointCooridnates[k]);
                    SecondPoint[k] = float.Parse(SecondPointCooridnates[k]);
                }
                line = new ArrowVisual3D();
                txt = new TextVisual3D();
                line.Diameter = 0.3;
                line.HeadLength = 10;
                line.Point1=new Point3D(FirstPoint[0], FirstPoint[1], FirstPoint[2]);
                line.Point2=new Point3D(SecondPoint[0], SecondPoint[1], SecondPoint[2]);
                if (IsWithColor)
                {
                    //colors.Add(Color.FromRgb((byte)rnd.Next(1, 255),
                    //(byte)rnd.Next(1, 255), (byte)rnd.Next(1, 255)));
                    var color= cmap.Colors().ElementAt(Normalization(i, MainG.Edges.Count()));
                    colors.Add(System.Windows.Media.Color.FromArgb(color.A,color.R,color.G,color.B));
                }
                if (MainG.Edges[i].WhiteEdge)
                {
                    line.Fill = new SolidColorBrush(Colors.White);
                    line.Diameter = 0.2;
                    line.BackMaterial = Materials.Gold;
                    line.Material = Materials.Gold;
                    line.Diameter = 0.05;
                }
                else
                {
                    line.Fill = new SolidColorBrush(colors[i]);
                }
                Model.Children.Add(line);
                txt.Text = (i + 1).ToString();
                //txt.Text = MainG.Edges[i].W.ToString();
                txt.Position = new Point3D((FirstPoint[0] + SecondPoint[0]) /2, (FirstPoint[1] + SecondPoint[1]) / 2, (FirstPoint[2] + SecondPoint[2]) / 2);
                //txt.FontSize = 18;
                txt.Height = 1;
                Model.Children.Add(txt);
                LinesOrder.Items.Add(txt.Text+" "+lineString);
            }
            LinesOrder.SelectedIndex=indx;
        }

        public void AddLine(string FirstPoint, string SecondPoint)
        {
            LinesOrder.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
            MainG.AddEdge(new Edge(FirstPoint, FirstPoint, 120, false));
            CreateScene(true, 0);
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

        private void LinesOrder_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (LinesOrder.SelectedIndex != -1)
            {
                LinesOrder.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
                ListBox list = (ListBox)sender;
                int indx = list.SelectedIndex;
                FirstSelectedPoint = MainG.Edges[indx].U;
                SecondSelectedPoint = MainG.Edges[indx].V;
                SelectedLineFirstPoint.Text = FirstSelectedPoint;
                SelectedLineSecondPoint.Text = SecondSelectedPoint; //consider do binding
            }
        }

        private void ChgDirectionButton_Click(object sender, RoutedEventArgs e)
        {
            LinesOrder.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
            int indx = LinesOrder.SelectedIndex;
            MainG.Edges[indx].Flip(true);
            CreateScene(false,indx);
        }

        private void Add_Line_Click(object sender, RoutedEventArgs e)
        {
            AddLine al = new AddLine();
            al.ShowDialog();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            LinesOrder.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
            int indx = LinesOrder.SelectedIndex;
            MainG.RemoveEdge(MainG.Edges[indx]);
            LinesOrder.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
            if (indx-1<0)
            { 
                CreateScene(false, 0);
            }
            else
            {
                CreateScene(false, indx-1);
            }
        }


        private void Optimize_Path(object sender, RoutedEventArgs e)
         {
             LinesOrder.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
             

             int indx = LinesOrder.SelectedIndex;
             Edge ar = new Edge(MainG.Edges[indx]);
             string st = ar.V;

             var answer = MainG.Postman(st);

            
             Graph optimizedG = new Graph();
             for (int i = 0; i < answer.Edges.Count; i++)
             {

                 var edge = answer.Edges.ElementAt(i);

                 switch (i)
                 {
                     case 0:
                         optimizedG.AddEdge(edge);
                         break;
                     case 1:
                         if (edge.U.Equals(optimizedG.Edges[0].U))
                         {
                             var temp = optimizedG.Edges[0];
                             optimizedG.RemoveEdge(temp);
                             optimizedG.AddEdge(temp.Flip());
                             optimizedG.AddEdge(edge);
                         }
                         else if (edge.U.Equals(optimizedG.Edges[0].V))
                         {
                             optimizedG.AddEdge(edge);
                         }
                         else
                         {
                             if (edge.V.Equals(optimizedG.Edges[0].U))
                             {
                                 var temp = optimizedG.Edges[0];
                                 optimizedG.RemoveEdge(temp);
                                 optimizedG.AddEdge(temp.Flip());
                                 optimizedG.AddEdge(edge.Flip());
                             }
                             else if (edge.V.Equals(optimizedG.Edges[0].V))
                             {
                                 optimizedG.AddEdge(edge.Flip());
                             }
                         }
                         break;
                     default:
                         if (optimizedG.Edges[i - 1].V.Equals(edge.U))
                         {
                             optimizedG.AddEdge(edge);
                         }
                       else 
                         {
                             optimizedG.AddEdge(edge.Flip());
                         }

                         break;
                 }               
             }
             MainG = optimizedG;
             CreateScene(true, 0);
         }

        private async void OptimizePrintingPath_Click(object sender, RoutedEventArgs e)
        {
            OptimizePrintingPath.IsEnabled = false;
            pts.IsPaused = false;
            int indx = 0;
            
                foreach (ArrowVisual3D seg in Model.Children.Where(p => p.GetType() == typeof(ArrowVisual3D)))
            {                
                seg.Fill = new SolidColorBrush(Color.FromRgb(128, 128, 128));
            }
                OnAnimation = !OnAnimation;
                DoubleAnimation anima = new DoubleAnimation();
                anima.To = 2;
                anima.From = 10;
                anima.Duration = TimeSpan.FromSeconds(0.9);
              //anima.RepeatBehavior = RepeatBehavior.Forever;
                foreach (ArrowVisual3D seg in Model.Children.Where(p => p.GetType() == typeof(ArrowVisual3D)))
                {
                if (indx==0)
                {
                    seg.Fill = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                    await Task.Delay(90);
                    seg.Fill = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                    await Task.Delay(90);
                    seg.Fill = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                    await Task.Delay(90);
                    seg.Fill = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                    await Task.Delay(90);                   
                    seg.Fill = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                    await Task.Delay(90);
                    seg.Fill = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                    await Task.Delay(90);
                    seg.Fill = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                    await Task.Delay(90);
                    seg.Fill = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                }
                //Brush b = seg.Fill;
                //seg.Fill = new SolidColorBrush(Color.FromRgb(0, 0, 255));
                seg.Material = new EmissiveMaterial(new SolidColorBrush(colors[indx]));
                // seg.AnimateOpacity(0.2, 0.2);

                await Task.Delay(animationTimeDelay);

                await pts.Token.WaitWhilePausedAsync();


                seg.BeginAnimation(ArrowVisual3D.HeadLengthProperty, anima);
                    //seg.Fill = b;
                   // seg.HeadLength = 10;
                seg.Diameter = seg.Diameter * 2;
                    LinesOrder.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
                indx++;
                }
            await Task.Delay(1000);
            CreateScene(true, 0);
            OptimizePrintingPath.IsEnabled = true;
            OnAnimation = !OnAnimation;         
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        private void PlayStop_Click(object sender, RoutedEventArgs e)
        {
            if (pts.IsPaused)
            {
                pts.IsPaused = false;
            }
            else
            {
                pts.IsPaused = true;
            }
        }
    }
    
}
