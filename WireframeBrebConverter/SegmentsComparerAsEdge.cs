﻿using MathNet.Numerics.LinearAlgebra;
using System.Collections.Generic;
using System;
using ChinskiListonosz.Core;

namespace WireframeBrebConverter
{
  

    public class SegmentsComparerAsEdge : IEqualityComparer<Edge>
    {
        public bool Equals(Edge x,Edge y)
        {
           
            // Return true if the fields match:
            return (
                    ((x.U == y.U && x.V == y.V) || (x.U == y.V && x.V ==y.U)));
        }

        public int GetHashCode(Edge obj)
        {
            int hashKey = obj.U.GetHashCode();
            int hashVal = obj.V.GetHashCode();
            return hashKey ^ hashVal;
        }
    }
}
