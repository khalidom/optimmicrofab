﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;

namespace WireframeBrebConverter
{
    /// <summary>
    /// Logique d'interaction pour AddLine.xaml
    /// </summary>
    public partial class AddLine : Window
    {
        public AddLine()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        public string X1 { get; set; }
        public string Y1 { get; set; }
        public string Z1 { get; set; }
        public string X2 { get; set; }
        public string Y2 { get; set; }
        public string Z2 { get; set; }

        private void Create_Line_Click(object sender, RoutedEventArgs e)
        {
            float[] FirstPoint = new float[3];
            float[] SecondPoint = new float[3];
            try
            {
                FirstPoint[0] =float.Parse( X1);
                FirstPoint[1] = float.Parse(Y1);
                FirstPoint[2] = float.Parse(Z1);
                SecondPoint[0] =float.Parse(X2);
                SecondPoint[1] =float.Parse(Y2);
                SecondPoint[2] =float.Parse(Z2);
                MainWindow mainWin = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
                if (mainWin != null)
                {
                    mainWin.AddLine(X1+"|"+Y1 + "|" + Z1 + "|", X2 + "|" + Y2 + "|" + Z2 + "|");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please verify the values of the coordinates for both points");
            }
           
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
